import qbs 1.0
import qbs.Environment

Project {
    name: "FMRadio"

    property string packageName: "it.mardy.fmradio"
    property string version: "0.2"

    qbsSearchPaths: "qbs"

    QtGuiApplication {
        name: "fmradio"
        version: project.version
        install: true

        cpp.cxxLanguageVersion: "c++14"
        cpp.defines: [
            'QT_DISABLE_DEPRECATED_BEFORE=0x050900',
            'QT_DEPRECATED_WARNINGS=0',
        ]
        cpp.enableExceptions: false

        Group {
            prefix: "src/"
            files: [
                "main.cpp",
            ]
        }

        Group {
            prefix: "src/qml/"
            files: [
                "ui.qrc",
            ]
        }

        Group {
            prefix: "data/"
            files: [
                "icons/icons.qrc",
                "fmradio.desktop",
            ]
        }

        Group {
            files: "data/fmradio.svg"
            fileTags: "freedesktop.appIcon"
        }

        Depends { name: "cpp" }
        Depends { name: "Qt.core" }
        Depends { name: "Qt.quick" }
        Depends { name: "Qt.quickcontrols2" }
        Depends { name: "Qt.svg" }
        Depends { name: "freedesktop" }

        freedesktop.name: product.name

        /*
         * Ubuntu Touch specific configuration
         */
        Depends {
            name: "ubuntutouch"
            condition: Environment.getEnv("TARGET_SYSTEM") == "UbuntuTouch"
        }

        Properties {
            condition: ubuntutouch.present
            freedesktop.desktopKeys: ubuntutouch.desktopKeys
            ubuntutouch.overrideDesktopKeys: ({
                "Icon": "./share/icons/hicolor/scalable/apps/fmradio.svg",
            })
            ubuntutouch.manifest: ({
                "version": project.version + "-0"
            })
        }

        Group {
            condition: ubuntutouch.present
            prefix: "data/ubuntu-touch/"
            files: [
                "manifest.json",
                "fmradio.apparmor",
            ]
        }
    }
}
