import QtMultimedia 5.8
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Page {
    id: root

    property var radioControl: null

    title: qsTr("Tuner not available")

    ColumnLayout {
        id: layout
        anchors { fill: parent; margins: 12 }


        Label {
            Layout.fillWidth: true
            text: qsTr("FM radio not available")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 32
            wrapMode: Text.WordWrap
        }

        Label {
            Layout.fillWidth: true
            visible: root.radioControl.availability == Radio.Unavailable
            wrapMode: Text.WordWrap
            textFormat: Text.RichText
            text: qsTr("There is no hardware support for FM radio on this device, or there are no drivers for it. You are welcome to visit the <a href=\"https://forums.ubports.com/\">UBports forums</a> to get help from other users, or you can check the <a href=\"https://gitlab.com/ubports/core/fm-radio-service/-/issues\">bug tracker</a> to see if an issue has been reported already.")
            onLinkActivated: Qt.openUrlExternally(link)
        }

        Label {
            Layout.fillWidth: true
            visible: root.radioControl.availability == Radio.Busy
            wrapMode: Text.WordWrap
            text: qsTr("The radio tuner is currently being used by another application.")
            onLinkActivated: Qt.openUrlExternally(link)
        }
        Label {
            Layout.fillWidth: true
            visible: root.radioControl.availability == Radio.ResourceMissing
            wrapMode: Text.WordWrap
            text: qsTr("The radio tuner is currently unavailable; please ensure that the headphones are connected and try again.")
            onLinkActivated: Qt.openUrlExternally(link)
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            Layout.bottomMargin: 12
            text: qsTr("Quit")
            highlighted: true
            onClicked: Qt.quit()
        }
    }
}
