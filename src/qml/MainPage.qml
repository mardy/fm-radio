import QtMultimedia 5.8
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Page {
    id: root

    property var radioControl: null

    title: qsTr("FM radio")

    ColumnLayout {
        id: layout
        anchors.fill: parent

        Item {
            Layout.fillWidth: true
            implicitWidth: frequencyLabel.implicitWidth
            implicitHeight: frequencyLabel.implicitHeight

            Label {
                id: frequencyLabel
                anchors.fill: parent
                text: qsTr("%1<br><small>KHz</small>").arg(root.radioControl.frequency / 1000.0)
                textFormat: Text.RichText
                font.pointSize: 26
                horizontalAlignment: Text.AlignHCenter
                visible: !frequencyInput.visible
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    frequencyInput.visible = true; frequencyInput.forceActiveFocus()
                }
            }

            TextInput {
                id: frequencyInput
                anchors.fill: parent
                inputMethodHints: Qt.ImhDigitsOnly
                visible: false
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 26
                onActiveFocusChanged: {
                    if (activeFocus) {
                        text = (root.radioControl.frequency / 1000).toString()
                    } else {
                        root.radioControl.frequency = parseInt(text, 10) * 1000
                        visible = false
                    }
                }
            }
        }

        ToolBar {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            padding: 2

            RowLayout {
                anchors.fill: parent

                ToolButton {
                    icon.source: "qrc:/icons/previous"
                    icon.color: "transparent"
                    onClicked: root.radioControl.scanDown()
                }
                ToolButton {
                    visible: root.radioControl.state == Radio.StoppedState
                    icon.source: "qrc:/icons/play"
                    icon.color: "transparent"
                    onClicked: root.radioControl.start()
                }
                ToolButton {
                    visible: root.radioControl.state == Radio.ActiveState
                    icon.source: "qrc:/icons/stop"
                    icon.color: "transparent"
                    onClicked: root.radioControl.stop()
                }
                ToolButton {
                    icon.source: "qrc:/icons/next"
                    icon.color: "transparent"
                    onClicked: root.radioControl.scanUp()
                }

                VolumeControl {
                    value: root.radioControl.volume / 100.0
                    onVolumeRequested: root.radioControl.volume = volume * 100
                }
            }
        }
    }
}
